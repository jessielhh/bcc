BCC
====

## Entrega: 10/07/2013

Especificação Trabalho 3 Redes 1
--------------------------------

	--> Criar/Emular uma rede Token Ring sobre a rede Ethernet do Dinf com 4 máquinas usando SOCKET DATAGRAMA (SOCK_DGRAM).
	--> Implementar um chat sobre a rede Token Ring SEM prioridade.
	--> Mensagens no chat podem viajar para TODAS as máquinas ou para uma máquina específica.
	--> Utilizar passagem de bastão temporizado.


									A---------->B
								 	^	     			|
									|						|
									|						|
									|						V
									D<----------C

	

