#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <limits.h>
#include <graphviz/cgraph.h>
#include "grafo.h"
#include <stdbool.h>

//------------------------------------------------------------------------------
const long int infinito = LONG_MAX;

//------------------------------------------------------------------------------
//Apontador para estrutura de arestas
typedef struct aresta *aresta;


//-----------------------------------------------------------------------------
typedef struct grafo {
  char *nome;
  lista vertices;
  long int diam;
  int numero_vertices, numero_arestas;
  bool pesado;
  bool direcionado;
} *grafo;

//------------------------------------------------------------------------------
typedef struct vertice {
  char *nome;
  lista vizinhos;
  bool visitado;
} *vertice;

struct aresta{
  vertice head, tail;
  long int peso;
  bool visitado;
};

//-----------------------------------------------------------------------------
// lista encadeada

typedef struct lista{
  no primeiro, ultimo;
} *lista;

//-----------------------------------------------------------------------------
// nó da lista encadeada cujo conteúdo é um void *

typedef struct no{
  void *conteudo;
  no proximo;
} *no;

//-----------------------------------------------------------------------------
static void ListaInsereNo(lista l, no n){
  if(l->ultimo == NULL){
    l->primeiro = n;
  }
  else{
    l->ultimo->proximo = n;
  }
  l->ultimo = n;
}

//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// devolve o primeiro nó da lista l,
//      ou NULL, se l é vazia

no primeiro_no(lista l){

  return l ? l->primeiro : NULL;

}

//------------------------------------------------------------------------------
// devolve o sucessor do nó n em l,
//      ou NULL, se n for o último nó de l

no proximo_no(no n){
  return n->proximo ? n->proximo : NULL;
}

//------------------------------------------------------------------------------
// devolve o conteúdo de n

void *conteudo(no n){
  return n->conteudo ? n->conteudo : NULL;
}


static char *AlocaString(char *string){
  char *string_ptr = (char *) malloc(strlen(string) * sizeof(char) + 1); //Aloca memoria para o string
  strcpy(string_ptr, string);

  return string_ptr;
}



//###################### Inicia Grafo ##########################################

static grafo inicia_grafo(Agraph_t *g_temp){

  grafo g = (grafo) malloc((size_t) sizeof(struct grafo)); //Aloca memoria para o grafo G

  g->vertices = (lista) malloc((long unsigned int)agnnodes(g_temp) * sizeof(struct lista)); // Aloca memoria para a lista de vertices
  g->vertices->primeiro = NULL;
  g->vertices->ultimo =NULL;

  g->nome = AlocaString(agnameof(g_temp));

  g->numero_vertices = agnnodes(g_temp);
  g->numero_arestas = agnedges(g_temp);
  g->direcionado = agisdirected(g_temp);
  g->pesado = false;
  g->diam = -1; //Diametro inicial do grafo utilizado para comparacoes posteriores

  return g;

}

//Funcao para buscar o indice do vertice utilizando o nome
//--------------------------------------------------------------------------------------------
static struct vertice *busca_vertice(grafo g, char *nome){

  for(no vertex = g->vertices->primeiro; vertex; vertex = vertex->proximo) {
    vertice v = vertex->conteudo; 
    if(!strcmp(v->nome, nome)){
      return v;
    }
  }
  return NULL;
}

//Funcao para salvar a lista de vertices
static void salva_vertices(grafo g, Agraph_t *g_temp){
  // carrega lista de vertices 


  for (Agnode_t *v=agfstnode(g_temp); v; v=agnxtnode(g_temp,v)) {

    no vertex = (no) malloc(sizeof(struct no));
 
    //Criar funcao para alocar vertice
    vertice ptr = (vertice) malloc (sizeof(struct vertice));
    ptr->nome =  AlocaString(agnameof(v));
    ptr->visitado = false;
    ptr->vizinhos = (lista) malloc (sizeof(struct lista));
    ptr->vizinhos->primeiro = NULL;
    ptr->vizinhos->ultimo = NULL;
    
    
    vertex->conteudo = ptr;
    vertex->proximo = NULL;

    ListaInsereNo(g->vertices, vertex);

  }

}

//Funcao para guardar a aresta de um grafo nao direcionado
//-----------------------------------------------------------------------------
static void guarda_arco(grafo g, Agraph_t *g_temp){

  aresta edge = (aresta) malloc (sizeof(struct aresta));

  char *peso;
  // Variavel para auxilar na verificacao se existem pesos nas arestas
  char s_peso[4] = "peso";
  

  for (Agnode_t *v = agfstnode(g_temp); v; v = agnxtnode(g_temp, v)) {
    for (Agedge_t *a = agfstout(g_temp, v); a; a = agnxtout(g_temp, a)) {
      edge->tail = busca_vertice(g, agnameof(agtail(a)));
      edge->head = busca_vertice(g, agnameof(aghead(a)));

      peso = agget(a, s_peso);

      if(peso){
        g->pesado = true;
        edge->peso = atol(peso);
      }
      else{
        edge->peso = 0;
      }

      no head = (no) malloc (sizeof(struct no));
      no tail = (no) malloc (sizeof(struct no));

      head->conteudo = edge;
      tail->conteudo = edge;
      head->proximo = NULL;
      tail->proximo = NULL;

      ListaInsereNo(edge->head->vizinhos, head);
      ListaInsereNo(edge->tail->vizinhos, tail);

    }
  }
 
}

//Funcao para guardar o arco de um grafo direcionado
//-----------------------------------------------------------------------------
static void guarda_aresta(grafo g, Agraph_t *g_temp){


  Agedge_t **arestas = malloc((size_t) g->numero_arestas * sizeof(Agedge_t *));
  int numero_arestas_temp = 0;
  bool encontrou;

  char *peso;
  // Variavel para auxilar na verificacao se existem pesos nas arestas
  char s_peso[4] = "peso";

  for (Agnode_t *v = agfstnode(g_temp); v; v = agnxtnode(g_temp, v)) {
    for (Agedge_t *a = agfstedge(g_temp, v); a; a = agnxtedge(g_temp, a, v)) {
      
      aresta edge = (aresta) malloc (sizeof(struct aresta));
      encontrou = false;
      edge->tail = busca_vertice(g, agnameof(agtail(a)));
      edge->head = busca_vertice(g, agnameof(aghead(a)));
      edge->visitado = false;
      
      for (int i = 0; i < numero_arestas_temp; i++){
        if(ageqedge(a, arestas[i])){
          encontrou = true;
        }
      }

      if(encontrou == false){

        arestas[numero_arestas_temp] = a;

        numero_arestas_temp++;

        peso = agget(a, s_peso);

        if(peso){
          g->pesado = true;
          edge->peso = atol(peso);
        }
        else{
          edge->peso = 0;
        }

        no head = (no) malloc (sizeof(struct no));
        no tail = (no) malloc (sizeof(struct no));

        head->conteudo = edge;
        tail->conteudo = edge;
        head->proximo = NULL;
        tail->proximo = NULL;
        

        ListaInsereNo(edge->head->vizinhos, head);
        ListaInsereNo(edge->tail->vizinhos, tail);
      }
    }
  }
}


//------------------------------------------------------------------------------
grafo le_grafo(FILE *input) {

  Agraph_t *g_temp = agread(input, NULL);

  if (!(g_temp && agisstrict(g_temp))){
    printf("ERRO: O grafo não é strict ou aconteceu um erro na abertura!!\n");
    exit(0);
  }
  
  grafo g = inicia_grafo(g_temp);

  salva_vertices(g, g_temp);

 if (g->direcionado) {
    guarda_arco(g, g_temp);
  }
  else{    
    guarda_aresta(g, g_temp);
  }

  agclose(g_temp);

  return g;
}


//------------------------------------------------------------------------------
int destroi_grafo(grafo g) {

    free(g->vertices);

  free(g->nome);

  free(g);

  return g ? 0 : 1;
}
//------------------------------------------------------------------------------
grafo escreve_grafo(FILE *output, grafo g) {

  char digraph[3] = "->\0";

  no vert, vert_aux;
  vertice v_1;

  fprintf(output, "strict %sgraph %s {\n\n",
         g->direcionado ? "di" : "",
         g->nome
         );

  for (vert_aux = g->vertices->primeiro; vert_aux; vert_aux = vert_aux->proximo){
    v_1 = vert_aux->conteudo;
    fprintf(output, "    %s\n", v_1->nome);
  }
  fprintf(output, "\n");



  for (vert_aux = g->vertices->primeiro; vert_aux ; vert_aux = vert_aux->proximo){
    v_1 = vert_aux->conteudo;
    for(vert = v_1->vizinhos->primeiro; vert; vert = vert->proximo){
      aresta a = vert->conteudo;
      if(a->visitado == false){
        fprintf(output, "    \"%s\" %s \"%s\"", a->tail->nome, g->direcionado ? digraph : "--", a->head->nome );
        a->visitado = true;
      if(g->pesado )
        fprintf(output, " [peso=%ld]\n", a->peso);
      else
        fprintf(output, "\n");
      }

    }
  }
  fprintf(output, "}\n");


  return output ? g : NULL;
}
//------------------------------------------------------------------------------
char *nome(grafo g) {

  return g ? g->nome : NULL;
}
//------------------------------------------------------------------------------
unsigned int n_vertices(grafo g) {

  return g ? (unsigned int)g->numero_vertices : 0;
}

//------------------------------------------------------------------------------
int direcionado(grafo g) {

  return g->direcionado;
}
//------------------------------------------------------------------------------
int conexo(grafo g) {

  return g->diam < infinito;
}
//------------------------------------------------------------------------------
int fortemente_conexo(grafo g)  {

  return g->direcionado && g->diam < infinito;
}