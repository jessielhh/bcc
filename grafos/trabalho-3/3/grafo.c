#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <graphviz/cgraph.h>
#include "grafo.h"
typedef struct aresta *aresta;

struct grafo {
	char *nome; // nome do grafo
	struct lista *vertices;
	int direcionado; // flag pra indicar se é direcionado
	int com_peso;
	int n_vertices, n_arestas; // número de vértices e arestas
	int strict; // indica se é strict (única diferença é se imprime "strict" na saída)
	int padding; // padding (warning do gcc)
	long int diametro; 
};
struct lista {
	int tam;
	int padding; // padding (warning do gcc)
	struct no *primeiro, *ultimo;
};
struct no {
	void *ptr;
	struct no *prox;
};
struct vertice {
	char *nome; // nome do vértice
	struct lista *vizinhos;
	int visitado; // pra não visitar 2 vezes nas buscas
	int index;  // pro Dijsktra
};
struct aresta {
	struct vertice *origem, *destino;
	long int peso;
	int visitada; // pra não imprimir 2 vezes
	int padding; // padding (warning do gcc)
};

//------------------------------------------------------------------------------
static void *alocar(long unsigned int size) {
	void *ptr = malloc((size_t) size);
	if (!ptr) {
		perror("erro no malloc\n");
		exit(EXIT_FAILURE);
	}

	return ptr;
}
static char *stralocpy(char *str) {
	char *ptr = alocar(sizeof(char) * strlen(str) + 1);
	strcpy(ptr, str);
	//ptr[0] = '\0';
	//strcat(ptr, str);

	return ptr;
}

// *********************************************************************
// FUNÇÕES PRA LISTA
static struct lista *aloca_lista(void) {
	lista l = alocar(sizeof(*l));
	l->primeiro = NULL;
	l->ultimo = NULL;
	l->tam = 0;

	return l;
}

static struct no *aloca_no(void *ptr) {
	no n = alocar(sizeof(*n));
	n->ptr = ptr;
	n->prox = NULL;

	return n;
}

static void insere_no(struct lista *l, struct no *n) {
	if (l->ultimo == NULL) {
		l->primeiro = n;
	} else {
		l->ultimo->prox = n;
	}
	l->ultimo = n;
	l->tam++;
}

static struct vertice *aloca_vertice(char *nome) {
	vertice v = alocar(sizeof(*v));
	v->nome = stralocpy(nome);
	v->vizinhos = aloca_lista();
	v->visitado = 0;

	return v;
}

static void insere_vertice(struct lista *l, char *nome) {
	vertice v = aloca_vertice(nome);
	no n = aloca_no(v);
	insere_no(l, n);
}

static struct vertice *acha_vertice(struct lista *l, char *nome) {
	no n = l->primeiro;
	while (n) {
		vertice v = n->ptr;
		if (!strcmp(v->nome, nome)) {
			return v;
		} else {
			n = n->prox;
		}
	}

	return NULL;
}

static int insere_aresta(struct lista *vertices, char *tail, char *head, char *peso) {
	aresta a = alocar(sizeof(*a));
	vertice o = acha_vertice(vertices, tail);
	vertice d = acha_vertice(vertices, head);
	if (!o || !d) {
		perror("vértices da aresta não achados\n");
		exit(EXIT_FAILURE);
	}
	a->origem = o;
	a->destino = d;
	a->visitada = 0;
	int com_peso;
	//printf("peso:%s\n", peso);
	if (peso && *peso) {
		a->peso = atol(peso);
		com_peso = 1;
	} else {
		a->peso = 0;
		com_peso = 0;
	}
	no n2 = aloca_no(a);
	no n3 = aloca_no(a);
	insere_no(o->vizinhos, n2);
	insere_no(d->vizinhos, n3);

	return com_peso;
}

static void copia_aresta(struct lista *vertices, aresta a) {
	char p[20];
	sprintf(p, "%ld", a->peso);
	insere_aresta(vertices, a->origem->nome, a->destino->nome, p);
}

// *********************************************************************
//  FUNÇÕES PÚBLICAS DA LISTA
no primeiro_no(lista l) {
	return l->primeiro;
}
no proximo_no(no n) {
	return n->prox;
}
void *conteudo(no n) {
	return n->ptr;
}


// *********************************************************************
// FUNÇÕES COPIADAS DO EXEMPLO (mas com parâmetros extras em vez de variáveis globais)
static int busca_aresta(Agedge_t *a, int n_arestas_visitadas, Agedge_t **arestas) {
	for (int i = 0; i < n_arestas_visitadas; ++i) {
		if (ageqedge(a, arestas[i])) {
			return 1;
		}
	}
	return 0;
}
static void guarda_arestas(Agraph_t *g, Agnode_t *v, int *n_arestas_visitadas, Agedge_t **arestas) {
	for (Agedge_t *a = agfstedge(g, v); a; a = agnxtedge(g, a, v)) {
		if (!busca_aresta(a, *n_arestas_visitadas, arestas)) {
			arestas[(*n_arestas_visitadas)] = a;
			*n_arestas_visitadas += 1;
		}
	}
}
static void guarda_arcos(Agraph_t *g, Agnode_t *v, int *n_arestas_visitadas, Agedge_t **arestas) {
	for (Agedge_t *a = agfstout(g, v); a; a = agnxtout(g, a)) {
		if (!busca_aresta(a, *n_arestas_visitadas, arestas)) {
			arestas[(*n_arestas_visitadas)] = a;
			*n_arestas_visitadas += 1;
		}
	}
}
// *********************************************************************
//------------------------------------------------------------------------------
int destroi_grafo(void *vg) {
	grafo g = vg;

	if (!g) {
		return 0;
	}
	
	no nv = g->vertices->primeiro;
	while (nv) {
		vertice v = nv->ptr;
		free(v->nome);

		no na = v->vizinhos->primeiro;
		while (na) {
			aresta a = na->ptr;
			if (a->visitada) {
				free(a);
				// cada aresta é visitada 2 vezes, uma na vizinhança da origem e outra na vizinhança do destino.
				// assim só tenta liberar a memória da aresta ao visitar pela segunda vez
			} else {
				a->visitada = 1;
			}
			no pa = na->prox;
			free(na);
			na = pa;
		}

		free(v->vizinhos);
		free(v);
		no pv = nv->prox;
		free(nv);
		nv = pv;
	}


	free(g->nome);
	free(g->vertices);
	free(g);
	
	return 1; 
}

static grafo aloca_grafo(char *nome) {
	grafo g = alocar(sizeof(*g));
	g->nome = stralocpy(nome);
	g->vertices = aloca_lista();

	return g;
}

grafo le_grafo(FILE *input) {
	Agraph_t *ag = agread(input, NULL);
	if (!ag) {
		return NULL;
	}
	grafo g = aloca_grafo(agnameof(ag));
	g->direcionado = agisdirected(ag);
	g->strict = agisstrict(ag);
	g->n_arestas = agnedges(ag);
	g->n_vertices = agnnodes(ag);

	// copia vértices e guarda as arestas temporariamente na variável local arestas[]
	int count = 0;
	int n_arestas_visitadas = 0;

	// vetor temporário pra guardar arestas usando a estrutura da biblioteca
	Agedge_t **arestas = alocar((size_t) agnedges(ag) * sizeof(Agedge_t *));

	for (Agnode_t *v = agfstnode(ag); v; v = agnxtnode(ag, v)) {
		insere_vertice(g->vertices, agnameof(v));
		if (g->direcionado) {
			guarda_arcos(ag, v, &n_arestas_visitadas, arestas);
		} else {
			guarda_arestas(ag, v, &n_arestas_visitadas, arestas);
		}
		count++;
	}

	// copia arestas
	g->com_peso = 0;
	for (int i = 0; i < n_arestas_visitadas; i++) {
		char *str = agget(arestas[i], (char*)"peso");
		if (insere_aresta(g->vertices, agnameof(agtail(arestas[i])),
					agnameof(aghead(arestas[i])), str)) {
			g->com_peso = 1;
		}
	}

	free(arestas);
	agclose(ag);

	return g;
}

//------------------------------------------------------------------------------
static void mostra_vertices(grafo g, FILE *output) {
	no v;
	for (v = g->vertices->primeiro; v; v = v->prox) {
		fprintf(output, "    \"%s\"\n", ((vertice) v->ptr)->nome);
	}
}

static void mostra_arestas(grafo g, FILE *output) {
	char rep_aresta = g->direcionado ? '>' : '-';
	no na, v;

	for (v = g->vertices->primeiro; v; v = v->prox) {
		for (na = ((vertice) v->ptr)->vizinhos->primeiro; na; na = na->prox) {
			aresta a = na->ptr;
			if (!a->visitada) {
				a->visitada = 1;
				fprintf(output, "    \"%s\" -%c \"%s\"",
						a->origem->nome,
						rep_aresta,
						a->destino->nome
					  );
				if (g->com_peso) {
					fprintf(output, " [peso=%ld]", a->peso);
				}
				fprintf(output, "\n");
			}
		}
	}
	for (v = g->vertices->primeiro; v; v = v->prox) {
		for (na = ((vertice) v->ptr)->vizinhos->primeiro; na; na = na->prox) {
			((aresta) na->ptr)->visitada = 0;
		}
	}
}

grafo escreve_grafo(FILE *output, grafo g) {
	if (!g) {
		return NULL;
	}
	//printf("com peso: %d\n", g->com_peso);
	fprintf(output, "%s%sgraph \"%s\" {\n\n",
		   g->strict ? "strict " : "",
		   g->direcionado ? "di" : "",
		   g->nome
		   );
	mostra_vertices(g, output);
	fprintf(output, "\n");
	mostra_arestas(g, output);
	fprintf(output, "}\n");

	return g;
}
//------------------------------------------------------------------------------

char *nome(grafo g) {
	return g->nome;
}

unsigned int n_vertices(grafo g) {
	return (unsigned) g->n_vertices;
}

int direcionado(grafo g) {
	return g->direcionado;
}

static void desvisita_arestas(grafo g) {
	for (no nv = g->vertices->primeiro; nv; nv = nv->prox) {
		vertice v = nv->ptr;
		for (no na = v->vizinhos->primeiro; na; na = na->prox) {
			aresta a = na->ptr;
			a->visitada = 0;
		}
	}
}
static void desvisita_vertices(grafo g) {
	for (no nv = g->vertices->primeiro; nv; nv = nv->prox) {
		vertice v = nv->ptr;
		v->visitado = 0;
	}
}

//------------------------------------------------------------------------------
static int busca_largura1(vertice v, int label) {
	if (v->visitado) {
		return 0;
	}
	v->visitado = label;
	for (no na = v->vizinhos->primeiro; na; na = na->prox) {
		aresta a = na->ptr;
		// TODO: funciona apenas pra grafos não-direcionados
		busca_largura1(a->origem, label);
		busca_largura1(a->destino, label);
	}

	return 1;
}
static void copia_vertice(grafo g, vertice vert) {
	vertice v = aloca_vertice(vert->nome);
	insere_no(g->vertices, aloca_no(v));
}

static void copia_arestas(grafo g, vertice vert) {
	for (no na = vert->vizinhos->primeiro; na; na = na->prox) {
		aresta a = na->ptr;
		if (!a->visitada) {
			copia_aresta(g->vertices, a);
			a->visitada = 1;
		}
	}
}

lista componentes(grafo g) {
	int label = 1; // v->visitado de cada vértice será o label do seu componente
	// precisa começar com 1 e não 0 porque 0 indica que não foi visitado ainda

	for (no n = g->vertices->primeiro; n; n = n->prox) {
		vertice v = n->ptr;
		if (busca_largura1(v, label)) {
			label++;
		}
	}

	label--;
	lista l_comps = aloca_lista();
	grafo *vetor_comps = alocar(sizeof(grafo) * (size_t) (label));
	char nome[20];
	for (int i = 0; i < label; i++) {
		sprintf(nome, "Componente %d", i + 1);
		grafo comp = aloca_grafo(nome);
		comp->direcionado = g->direcionado;
		comp->com_peso = g->com_peso;
		comp->strict = g->strict;
		insere_no(l_comps, aloca_no(comp));
		vetor_comps[i] = comp;
	}

	for (no n = g->vertices->primeiro; n; n = n->prox) {
		vertice v = n->ptr;
		copia_vertice(vetor_comps[v->visitado - 1], v);
	}
	for (no n = g->vertices->primeiro; n; n = n->prox) {
		vertice v = n->ptr;
		copia_arestas(vetor_comps[v->visitado - 1], v);
		v->visitado = 0;
	}
	desvisita_arestas(g);
	free(vetor_comps);

	return l_comps;
}
//------------------------------------------------------------------------------
static vertice outro_vertice(aresta a, vertice v) {
	if (a->origem == v) {
		return a->destino;
	} else {
		return a->origem;
	}
}

static void busca_gulosa(grafo novo, vertice v) {
	if (v->visitado) {
		return;
	}

	v->visitado = 1;
	no na = v->vizinhos->primeiro;
	aresta a = na->ptr;
	while (outro_vertice(a, v)->visitado) {
		na = na->prox;
		if (!na) return;
		a = na->ptr;
	}

	aresta menor_peso = a;
	na = na->prox;
	while (na) {
		aresta b = na->ptr;
		if (!outro_vertice(b, v)->visitado && menor_peso->peso > b->peso) {
			menor_peso = b;
		}
		na = na->prox;
	}

	copia_vertice(novo, outro_vertice(menor_peso, v));
	copia_aresta(novo->vertices, menor_peso);
	busca_gulosa(novo, outro_vertice(menor_peso, v));

	return;
}

grafo arvore_geradora_minima(grafo g) {
	// Prim's algorithm
	if (g->direcionado || !g->com_peso) {
		return NULL;
	}
	// if diametro == infinito return NULL?

	grafo novo = aloca_grafo(g->nome);
	novo->direcionado = 0;
	novo->com_peso = 1;
	vertice v = (vertice) g->vertices->primeiro->ptr;
	copia_vertice(novo, v);
	busca_gulosa(novo, v);

	return novo;
}
//------------------------------------------------------------------------------
int conexo(grafo g) {
	// TODO: diametro?
	return g->diametro != infinito && !g->direcionado;
}
int fortemente_conexo(grafo g) {
	return g->diametro != infinito && g->direcionado;
}
long int diametro(grafo g) {
	return g->diametro;
}
//------------------------------------------------------------------------------
lista ordena(grafo g) {
	// G NÃO PODE TER CICLOS TAMBÉM
	// TODO ordenação não é única?
	if (!g->direcionado) {
		return NULL;
	}
	return g->vertices;
}
//------------------------------------------------------------------------------
vertice primeiro_vertice(grafo g) {
	// pra testar
	return g->vertices->primeiro->ptr;
}
static aresta menor_dist_de_um_visitado(vertice v) {
	// v é um vértice não visitado ainda
	// aresta retornada é a de menor peso de um vértice visitado até v
	aresta menor_peso = NULL;

	for (no na = v->vizinhos->primeiro; na; na = na->prox) {
		aresta a = na->ptr;
		// assume que o grafo é direcionado!
		if (a->destino == v && a->origem->visitado) {
			if (!menor_peso || a->peso < menor_peso->peso) {
				menor_peso = a;
			}
		}
	}

	return menor_peso;
}

static aresta proxima_aresta(grafo g, long int *distancias) {
	// retorna próxima aresta de um vértice visitado até um não visitado 
	// com menor distância total a partir da raiz
	aresta p = NULL;
	long int dist = infinito;

	for (no nv = g->vertices->primeiro; nv; nv = nv->prox) {
		vertice v = nv->ptr;
		if (!v->visitado) {
			aresta a = menor_dist_de_um_visitado(v);
			if (a) {
				long int nova_dist = a->peso + distancias[a->origem->index];
				if (!p || (nova_dist < dist)) {
					p = a;
					dist = nova_dist;
				}
			}
		}
	}

	return p;
}

grafo arborescencia_caminhos_minimos(grafo g, vertice r) {
	// Assumir que g é direcionado acíclico com pesos?
	char str[20] = "Dijsktra";
	grafo t = aloca_grafo(str);
	t->strict = g->strict;
	t->direcionado = g->direcionado;
	t->com_peso = g->com_peso;

	// distancias[v->index] indica distancia da raiz até v
	long int *distancias = alocar(sizeof(long int) * (size_t) g->n_vertices);
	for (int i = 0; i < g->n_vertices; i++) {
		distancias[i] = infinito;
	}
	int i = 0;
	for (no nv = g->vertices->primeiro; nv; nv = nv->prox) {
		vertice v = nv->ptr;
		v->index = i;
		i++;
	}

	copia_vertice(t, r);
	distancias[r->index] = 0;
	r->visitado = 1;
	int visitados = 1;

	while (visitados != g->n_vertices) {
		aresta p = proxima_aresta(g, distancias);
		copia_vertice(t, p->destino);
		p->destino->visitado = 1;
		copia_aresta(t->vertices, p);
		visitados++;
	}

	free(distancias);
	desvisita_vertices(g);

	return t;
}

//------------------------------------------------------------------------------
