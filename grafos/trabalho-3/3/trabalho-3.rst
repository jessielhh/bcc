Trabalho de Implementação 3
===========================

CI065 / CI755 - Algoritmos e Teoria dos Grafos
----------------------------------------------

Neste trabalho vamos implementar alguns dos algoritmos baseados em
busca discutidos em aula.

O arquivo `grafo.h <grafo.h>`__ contém a especificação do que deve ser
implementado, do mesmo modo que nos trabalhos anteriores.

Observe que, ao contrário do `trabalho 2 <trabalho-2.html>`__, a
implementação dos grafos neste trabalho deve ser feita através de
listas de adjacência para permitir a implementação eficiente dos
algoritmos de busca.

-----

O  trabalho deve  ser  entregue sob  a  forma de  um  arquivo de  nome
``fulano-sicrano.tar.gz``,  sendo que  ``fulano`` e  ``sicrano`` devem
ser     substituídos     pelos    `identificadores     dos     autores
<http://www.inf.ufpr.br/renato/ci065>`__ constantes na `página da
turma <http://www.inf.ufpr.br/renato/ci065>`__.

O arquivo ``fulano-sicrano.tar.gz``, uma  vez expandido, deve criar 
(somente) os seguintes arquivos.

:``fulano-sicrano/readme.txt``: arquivo  que   deve  ser   usado  para
                                comunicar tudo que seja relevante para
                                a correção do trabalho.

:``fulano-sicrano/grafo.c``: a   implementação   do  especificado   em
                             `grafo.h <grafo.h>`__

-----

O arquivo  ``fulano-sicrano.tar.gz`` deve  ser entregue como  anexo de
mensagem  enviada  para renato.carmo.rc@gmail.com.   O  "``Subject:``"
desta mensagem deve ser "``Entrega do trabalho 3``".

O prazo para a entrega é  **21/6/2015, às 23h59min**.

-----

.. _faq:

Perguntas Frequentes
--------------------

#. *O trabalho pode ser feito em grupo?*

   O trabalho pode ser feito em duplas ou individualmente.

   No caso de  trabalhos individuais o nome do arquivo  a ser entregue
   deve  ser   ``fulano.tar.gz``,  sendo   que  ``fulano``   deve  ser
   substituido        pelo        `identificador       do        autor
   <http://www.inf.ufpr.br/renato/ci065>`__.

#. *Tenho que fazer este trabalho com o mesmo colega (individualmente)
   com quem (como) fiz os trabalhos anteriores?*

   Não. Você pode escolher fazer individualmente ou em dupla com o
   mesmo colega ou outro independentemente de como fez os trabalhos
   anteriores.
   
#. *O arquivo*  ``grafo.c`` *pode conter  outras funções/variáveis/tipos
   além daqueles presentes no esqueleto fornecido?*

   Pode (e, para boa organização do código, deve).  Como usual, a
   especificação do trabalho descreve somente a interface a ser
   implementada. A maneira de implementar é livre.
   
#. *Devo usar a estrutura de dados implementada nos trabalhos anteriores?*

   Você pode, mas isso não é obrigatório e, dependendo de sua
   implementação nos trabalhos anteriores, pode nem ser adequado.

#. *O que será levado em conta na correção?*

   Na correção serão levados em conta (entre outros) os seguintes elementos.

   #. Conformidade com a especificação.

   #. Correção da implementação.

   #. Organização e clareza  do código (nomes de  funções e variáveis,
      comentários etc).

   #. Eficiência da implementação.


#. *Por que a especificação de entrega é importante?*

   Porque o trabalho entregue será  pré-processado por um programa que
   depende de a especificação de entrega ser corretamente observada.

#. *O que acontece se a  especificação de entrega não for corretamente
   observada?*

   Seu trabalho  só será corrigido  quando o professor tiver  tempo de
   fazer manualmente  o pré-processamento. Por isso,  haverá um severo
   desconto na sua nota, proporcional ao trabalho de pré-processamento
   que tenha que ser feito manualmente.

#. *Descobri um  erro depois que  entreguei o trabalho.  Posso entregar
   uma versão corrigida?*

   Você  pode entregar  o  trabalho mais  de uma  vez.   A última  versão
   entregue dentro do prazo é a que será corrigida.

#. *Meu trabalho tem um bug. O que vai acontecer com minha nota?*

   Haverá algum desconto, dependendo da  gravidade do bug.  O desconto
   será  menor  se  o  bug for  relatado  no  arquivo  ``readme.txt``,
   indicando que você estava ciente do problema quando entregou.

#. *Tenho outra pergunta/dúvida a respeito do trabalho.*

   Envie mensagem para a `lista da disciplina <mailto:Algoritmos%20e%20Teoria%20dos%20Grafos%20%3Cci065@listas.inf.ufpr.br%3E>`__.




