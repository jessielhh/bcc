#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <graphviz/cgraph.h>
#include "math.h"
#include "grafo.h"


//Apontador para estrutura dos vertices
typedef struct vertice *vertice;

//Apontador para estrutura de arestas
typedef struct aresta *aresta;


//Salva vertices para posteriormente serem usados
void salva_vertices(vertice *vertices, Agraph_t *g_temp);


//Salva arestas
void salva_arestas(aresta *arestas, Agraph_t *g_temp, vertice *vertices);

//Busca vertice para adicionar como head da aresta
vertice busca_vertice(vertice *v, char *nome);

static int direcionado, numero_arestas, numero_vertices;


#ifndef DBL_EPSILON

#define DBL_EPSILON 1E-5
  
#endif


//------------------------------------------------------------------------------
// estrutura de dados para representar um grafo simples
// 
// o grafo pode ser
// - direcionado ou não
// - com pesos nas arestas ou não
// 
// além dos vértices e arestas, o grafo tem um nome
// 
// os vértices do grafo também tem nome
// 
// os nomes do grafo e dos vértices são strings quaisquer
// 
// num grafo com pesos nas arestas todas as arestas tem peso, que é um double
// 
// o peso default de uma aresta é 0.0

struct grafo {
	char *nome;
	vertice *vertices;
	aresta *arestas;
};

struct vertice {
	char nome[64];
};

struct aresta {
	double peso;
	vertice head;
	vertice tail;
};

//------------------------------------------------------------------------------
// lê um grafo no formato dot de input, usando as rotinas de libcgraph
// 
// desconsidera todos os atributos do grafo lido
// exceto o atributo "peso" nas arestas onde ocorra
// 
// num grafo com pesos nas arestas todas as arestas tem peso, que é um double
// 
// o peso default de uma aresta num grafo com pesos é 0.0
// 
// devolve o grafo lido ou
//         NULL em caso de erro 


vertice busca_vertice(vertice *v, char *nome){

	for (int i=0; i < numero_vertices; i++) {

  	if(!strcmp(v[i]->nome, nome)){
  		return v[i];
  	}
  }
  return NULL;
}

void salva_vertices(vertice *vertices, Agraph_t *g_temp){
  // carrega lista de vertices 
  int i=0;

  for (Agnode_t *v=agfstnode(g_temp); v; v=agnxtnode(g_temp,v)) {
    vertices[i] = (vertice) malloc (sizeof(struct vertice));
    strcpy(vertices[i]->nome, agnameof(v));
    i++;
  }
}

void salva_arestas(aresta *arestas, Agraph_t *g_temp, vertice *vertices){

  int i=0;
  int j=0;
  char *peso;
  // Variavel para auxilar na verificacao se existem pesos nas arestas
  char *s_peso = malloc(sizeof(char)*4);
  strcpy(s_peso, "peso");
  //-----------------------------------------------------------------

  for (Agnode_t *v=agfstnode(g_temp); v; v=agnxtnode(g_temp,v)) {
    if (agdegree(g_temp, v, 0, 1)){
      for (Agedge_t *a=agfstout(g_temp,v); a; a=agnxtout(g_temp,a)){
        arestas[j]= (aresta) malloc (sizeof(struct aresta));

        arestas[j]->tail = vertices[i];
        arestas[j]->head = busca_vertice(vertices, agnameof(aghead(a)));
        
        peso = agget(a, s_peso);

        if(peso){
          arestas[j]->peso = atof(peso);
        }

        j++;
      }
    }
    i++;
  }
}


grafo le_grafo(FILE *input) {

	aresta *arestas;
	vertice *vertices; 
  // alocacao do grafo
	grafo g = (grafo) malloc((size_t) sizeof(struct grafo));

  //Leitura do grafo vindo do arquivo .dot
	Agraph_t *g_temp = agread(input, NULL);
	
	if ( ! (g_temp && agisstrict(g_temp)) )
    return NULL;
  
  //Copiando o nome do grafo
  g->nome = (char*)malloc(strlen(agnameof(g_temp)));
  strcpy(g->nome, agnameof(g_temp));
 
  direcionado = agisdirected(g_temp);

  numero_vertices = agnnodes(g_temp);

  numero_arestas = agnedges(g_temp);

  vertices = (vertice *) malloc((size_t) numero_vertices * sizeof(struct vertice));

  salva_vertices(vertices, g_temp);

  arestas = (aresta *) malloc((size_t) numero_arestas * sizeof(struct aresta));

  salva_arestas(arestas, g_temp, vertices);

  g->vertices = vertices;
  g->arestas = arestas;
  free(g_temp);
	return g;



}
//------------------------------------------------------------------------------
// desaloca toda a memória utilizada em g
// 
// devolve 1 em caso de sucesso ou
//         0 em caso de erro

int destroi_grafo(grafo g) {

	free(g->vertices);
	free(g->arestas);
	free(g);

	if(!g)
		return 1;
	else
		return 0;


}
//------------------------------------------------------------------------------
// escreve o grafo g em output usando o formato dot, de forma que
// 
// 1. todos os vértices são escritos antes de todas as arestas/arcos 
// 2. se uma aresta tem peso, este deve ser escrito como um atributo
//
// devolve o grafo escrito ou
//         NULL em caso de erro 

grafo escreve_grafo(FILE *output, grafo g) {


	char digraph[3] = "->\0";

	fprintf(output, "strict %sgraph \"%s\" {\n\n",
         direcionado ? "di" : "",
         g->nome
         );

	for (int i=0; i < numero_vertices; i++){
		fprintf(output, "\t\"%s\"\n", g->vertices[i]->nome);
	}
	fprintf(output, "\n");
	for (int j=0 ; j < numero_arestas; j++){
	  fprintf(output, "\t\"%s\" %s \"%s\"", g->arestas[j]->tail->nome, direcionado ? digraph : "--" , g->arestas[j]->head->nome );
		
		if(fabs(g->arestas[j]->peso) > DBL_EPSILON )
			fprintf(output, " [peso=%.1f]\n", g->arestas[j]->peso);
	  else
      fprintf(output, "\n");
  }

	fprintf(output, "}\n");	

	return g;
}

