#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <limits.h>
#include <graphviz/cgraph.h>
#include "grafo.h"
#include <stdbool.h>

//------------------------------------------------------------------

//------------------------------------------------------------------------------
typedef struct grafo {
  char *nome;
  vertice *vertices;
  int **matriz_adjacencia;
  long int **matriz_distancias;
  long int **matriz_pesos;
  long int diam;
  int numero_vertices;
  bool pesado;
  bool direcionado;
  bool padding[2];
} *grafo;

//------------------------------------------------------------------------------
typedef struct vertice {
  char *nome;
} *vertice;

//------------------------------------------------------------------------------
const long int infinito = LONG_MAX;

static int **mem_matr(grafo g){

  int **m_aux = (int **) malloc((long unsigned int)g->numero_vertices * sizeof(int *)); 
  for(int i = 0; i < g->numero_vertices; i++){
    m_aux[i] = (int *) malloc((long unsigned int)g->numero_vertices * sizeof(int));
    for(int j = 0; j < g->numero_vertices; j++){
      m_aux[i][j] = 0;
    }
  }
  return m_aux;
}

static long int **mem_long_matr(grafo g, long int ref){

  long int **m_aux = (long int **) malloc((long unsigned int)g->numero_vertices * sizeof(long int *)); 
  for(int i = 0; i < g->numero_vertices; i++){
    m_aux[i] = (long int *) malloc((long unsigned int)g->numero_vertices * sizeof(long int));
    for(int j = 0; j < g->numero_vertices; j++){
      m_aux[i][j] = ref;
    }
  }
  return m_aux;
}




//###################### Inicia Grafo ##########################################

static grafo inicia_grafo(char *nome, bool direcionado, int numero_vertices){

  grafo g = (grafo) malloc((size_t) sizeof(struct grafo));

  g->vertices = (vertice *) malloc((long unsigned int)numero_vertices * sizeof(struct vertice));


  g->nome = (char *) malloc(strlen(nome) * sizeof(char)+1);
  strcpy(g->nome, nome);

  g->numero_vertices = numero_vertices;
  g->direcionado = direcionado;
  g->pesado = false;
  g->diam = -1;


  g->matriz_distancias = mem_long_matr(g, -1);

  g->matriz_adjacencia = mem_matr(g);

  g->matriz_pesos = mem_long_matr(g, 0);



  return g;

}





//--------------------------------------------------------------------------------------------

static int busca_vertice(grafo g, char *nome){

  for (int i=0; i < g->numero_vertices; i++) {

    if(!strcmp(g->vertices[i]->nome, nome)){
      return i;
    }
  }
  return 0;
}

static void salva_vertices(grafo g, Agraph_t *g_temp){
  // carrega lista de vertices 
  int i=0;

  for (Agnode_t *v=agfstnode(g_temp); v; v=agnxtnode(g_temp,v)) {
  
    g->vertices[i] = (vertice) malloc (sizeof(struct vertice));
    g->vertices[i]->nome = (char *) malloc (sizeof(char) * strlen(agnameof(v)) + 1);
    strcpy(g->vertices[i]->nome, agnameof(v));
    i++;
  }
}


//-----------------------------------------------------------------------------
static void guarda_aresta(grafo g, Agraph_t *g_temp){

  int i;
  int j;

  // Variavel para auxilar na verificacao se existem pesos nas arestas
  char s_peso[4] = "peso";
  char *peso;

  for (Agnode_t *v = agfstnode(g_temp); v; v = agnxtnode(g_temp, v)) {
    for (Agedge_t *a = agfstout(g_temp, v); a; a = agnxtout(g_temp, a)) {
        i = busca_vertice(g, agnameof(agtail(a)));
        j = busca_vertice(g, agnameof(aghead(a)));


        g->matriz_adjacencia[i][j] = 1;

        g->matriz_adjacencia[j][i] = 1;

        peso = agget(a, s_peso);

        if(peso){
          g->pesado = true;
          g->matriz_pesos[i][j] = atol(peso);
          g->matriz_pesos[j][i] = atol(peso);
        }

      }
    }
    free(peso);
}
//-----------------------------------------------------------------------------
static void guarda_arco(grafo g, Agraph_t *g_temp){

  int i;
  int j;

  char *peso;
  // Variavel para auxilar na verificacao se existem pesos nas arestas
  char s_peso[4] = "peso";

  for (Agnode_t *v = agfstnode(g_temp); v; v = agnxtnode(g_temp, v)) {
    for (Agedge_t *a = agfstedge(g_temp, v); a; a = agnxtedge(g_temp, a, v)) {
        i = busca_vertice(g, agnameof(agtail(a)));
        j = busca_vertice(g, agnameof(aghead(a)));

        g->matriz_adjacencia[i][j] = 1;

        peso = agget(a, s_peso);

        if(peso){
          g->pesado = true;
          g->matriz_pesos[i][j] = atol(peso);
        }

      }
    }
}


// A utility function to find the vertex with minimum distance value, from
// the set of vertices not yet included in shortest path tree
static int minDistance(long int dist[], bool sptSet[], grafo g)
{
   // Initialize min value
   long int min = infinito;
   int min_index;
 
   for (int v = 0; v < g->numero_vertices; v++)
     if (sptSet[v] == false && dist[v] <= min)
         min = dist[v], min_index = v;
 
   return min_index;
}
 
// Funtion that implements Dijkstra's single source shortest path algorithm
// for a graph represented using adjacency matrix representation
static void dijkstra(grafo g)
{

 
  bool *sptSet = (bool *) malloc((long unsigned int)g->numero_vertices * sizeof(bool)); // sptSet[i] will true if vertex i is included in shortest
                     // path tree or shortest distance from src to i is finalized
 
  for(int src = 0; src < g->numero_vertices; src++){

    // Initialize all distances as INFINITE and stpSet[] as false
    for (int i = 0; i < g->numero_vertices ; i++)
      g->matriz_distancias[src][i] = infinito, sptSet[i] = false;

      // Distance of source vertex from itself is always 0
      g->matriz_distancias[src][src] = 0;

    // Find shortest path for all vertices
    for (int count = 0; count < g->numero_vertices-1; count++){
      // Pick the minimum distance vertex from the set of vertices not
      // yet processed. u is always equal to src in first iteration.
      int u = minDistance(g->matriz_distancias[src], sptSet, g);

      // Mark the picked vertex as processed
      sptSet[u] = true;
      // Update dist value of the adjacent vertices of the picked vertex.
      for (int v = 0; v < g->numero_vertices; v++)

        // Update dist[v] only if is not in sptSet, there is an edge from
        // u to v, and total weight of path from src to  v through u is
        // smaller than current value of dist[v]
        if (!sptSet[v] && g->matriz_adjacencia[u][v] && g->matriz_distancias[src][u] != infinito
                                      && g->matriz_distancias[src][u]+g->matriz_adjacencia[u][v] < g->matriz_distancias[src][v])
          g->matriz_distancias[src][v] = g->matriz_distancias[src][u] + g->matriz_adjacencia[u][v];
    }
  }

  free(sptSet);

}




//------------------------------------------------------------------------------
grafo le_grafo(FILE *input) {

  Agraph_t *g_temp = agread(input, NULL);

  if (!(g_temp && agisstrict(g_temp))){
    printf("ERRO: O grafo não é strict!!\n");
    exit(0);
}
  
  grafo g = inicia_grafo(agnameof(g_temp), agisdirected(g_temp), agnnodes(g_temp));

  salva_vertices(g, g_temp);



  if (g->direcionado) {
    guarda_arco(g, g_temp);
  }
  else{    
    guarda_aresta(g, g_temp);
  }

  

  agclose(g_temp);

  return g;
}


//------------------------------------------------------------------------------
int destroi_grafo(grafo g) {

  for(int i = 0; i < g->numero_vertices; i++){
    free(g->matriz_adjacencia[i]);
    free(g->matriz_pesos[i]);
    free(g->matriz_distancias[i]);
  }

  free(g->matriz_adjacencia);
  free(g->matriz_pesos);
  free(g->matriz_distancias);
  

  for(int i = 0; i < g->numero_vertices; i++){
    free(g->vertices[i]->nome);
    free(g->vertices[i]);
  }
  free(g->vertices);

  free(g->nome);

  free(g);

  return g ? 0 : 1;
}
//------------------------------------------------------------------------------
grafo escreve_grafo(FILE *output, grafo g) {

  char digraph[3] = "->\0";

  fprintf(output, "strict %sgraph %s-distancias {\n\n",
         g->direcionado ? "di" : "",
         g->nome
         );

  for (int i=0; i < g->numero_vertices; i++){
    fprintf(output, "    %s\n", g->vertices[i]->nome);
  }
  fprintf(output, "\n");

  for (int i = 0; i < g->numero_vertices; i++ ){
    for (int j = g->direcionado ? 0 : i; j < g->numero_vertices; j++) {
      fprintf(output, "    %s %s %s", g->vertices[i]->nome, g->direcionado ? digraph : "--" , g->vertices[j]->nome );
      
      if(g->pesado )
        fprintf(output, " [peso=%ld]\n", g->matriz_pesos[i][j]);
      else
        fprintf(output, "\n");
    }
  }

  fprintf(output, "}\n"); 

  return output ? g : NULL;
}
//------------------------------------------------------------------------------
char *nome(grafo g) {

  return g ? g->nome : NULL;
}
//------------------------------------------------------------------------------
unsigned int n_vertices(grafo g) {

  return g ? (unsigned int)g->numero_vertices : 0;
}

//------------------------------------------------------------------------------
int direcionado(grafo g) {

  return g->direcionado;
}
//------------------------------------------------------------------------------
int conexo(grafo g) {

  return g->diam < infinito;
}
//------------------------------------------------------------------------------
int fortemente_conexo(grafo g)  {

  return g->direcionado && g->diam < infinito;
}
//------------------------------------------------------------------------------
long int diametro(grafo g) {

  if (g->diam == -1){
    dijkstra(g);
    for (int i = 0; i < g->numero_vertices; i++) {
      for (int j = 0; j < g->numero_vertices; j++) {
        if(g->matriz_distancias[i][j] > g->diam)
          g->diam = g->matriz_distancias[i][j];
      }
    }
    
  } 
  return g->diam;
}
//------------------------------------------------------------------------------

grafo distancias(grafo g) {

    // precisa da matriz de distâncias
  if(!g->matriz_distancias)
    dijkstra(g);
  grafo g_aux = inicia_grafo(g->nome, g->direcionado, g->numero_vertices);

  g_aux->diam = g->diam;
  g_aux->pesado = true;
  // primeiro copia todos os vértices
  for (int i = 0; i < g->numero_vertices; i++) {
  
    g_aux->vertices[i] = (vertice) malloc (sizeof(struct vertice));
    g_aux->vertices[i]->nome = (char *) malloc (sizeof(char) * strlen(g->vertices[i]->nome) + 1);
    strcpy(g_aux->vertices[i]->nome, g->vertices[i]->nome);
  }

  // depois coloca todas (grafo completo) as arestas na matriz
  // de adjacência e as distâncias na de peso
  for (int i = 0; i < g->numero_vertices; i++) {
    for (int j = 0; j < g->numero_vertices; j++) {
      g_aux->matriz_adjacencia[i][j] = 1;
      g_aux->matriz_pesos[i][j] = g->matriz_distancias[i][j];
    }
  }

  return g_aux;
}
