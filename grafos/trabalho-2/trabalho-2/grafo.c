#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <limits.h>
#include <graphviz/cgraph.h>
#include "grafo.h"
#include <stdbool.h>

//------------------------------------------------------------------

// Number of vertices in the graph
#define V 9

static int direct, numero_arestas, numero_vertices;

void salva_vertices(vertice *vertices, Agraph_t *g_temp);

void salva_arestas(vertice *vertices, Agraph_t *g_temp, grafo g);

int busca_vertice(vertice *vertices, char *nome);

int minDistance(long int dist[], bool sptSet[]);

void printSolution(long int dist[], int n);

void dijkstra(grafo g, int src);

//------------------------------------------------------------------------------
typedef struct grafo {
  char *nome;
  vertice *vertices;
  int **matriz_adjacencia;
} *grafo;

//------------------------------------------------------------------------------
typedef struct vertice {
  char nome[64];
} *vertice;

//------------------------------------------------------------------------------
const long int infinito = LONG_MAX;

//------------------------------------------------------------------------------
grafo le_grafo(FILE *input) {

  vertice *vertices;

  grafo g = (grafo) malloc((size_t) sizeof(struct grafo));

  Agraph_t *g_temp = agread(input, NULL);

  if (!(g_temp && agisstrict(g_temp)))
    return NULL;

  g->nome = (char*) malloc(strlen(agnameof(g_temp) + (sizeof(char) * 10)));

  strcpy(g->nome, agnameof(g_temp));

  direct = agisdirected(g_temp);
  numero_vertices = agnnodes(g_temp);
  numero_arestas = agnedges(g_temp);

  vertices = (vertice *) malloc((size_t) numero_vertices * sizeof(struct vertice));

  salva_vertices(vertices, g_temp);


  salva_arestas(vertices, g_temp, g);

  for(int i =0; i < numero_vertices; i++)
    dijkstra(g, i);

  g->vertices = vertices;

  return g;
}
//--------------------------------------------------------------------------------------------

int busca_vertice(vertice *vertices, char *nome){

  for (int i=0; i < numero_vertices; i++) {

    if(!strcmp(vertices[i]->nome, nome)){
     // printf("(%d) %s -- ", i, vertices[i]->nome);
      return i;
    }
  }
  return 0;
}

//------------------------------------------------------------------------------
void salva_arestas(vertice *vertices, Agraph_t *g_temp, grafo g){

  int i;
  int j;
  g->matriz_adjacencia = (int **) malloc((long unsigned int)numero_vertices * sizeof(int *));

  for (i = 0; i < numero_vertices; i++){
    g->matriz_adjacencia[i] = (int*) malloc((long unsigned int)numero_vertices * sizeof(int));
    for(j = 0; j < numero_vertices; j++){
      g->matriz_adjacencia[i][j] = 0;
    }
  }

  for (Agnode_t *v=agfstnode(g_temp); v; v=agnxtnode(g_temp,v)) {
      for (Agedge_t *a=agfstout(g_temp,v); a; a=agnxtout(g_temp,a)){
        //buscar vertice i
        //buscar vertice tail j
        // nao direcionada marca os dois
        i = busca_vertice(vertices, agnameof(agtail(a)));
        j = busca_vertice(vertices, agnameof(aghead(a)));

       // printf("\n");

        g->matriz_adjacencia[i][j] = 1;

        g->matriz_adjacencia[j][i] = 1;
      }
    
  }
/*  printf (" \t");
  for (i = 0; i < numero_vertices; i++){
    printf("%d ",i);
  }
  for (i = 0; i < numero_vertices; i++){
    printf ("\n%d\t", i);

    for(j = 0; j < numero_vertices; j++){
      printf("%d ", g->matriz_adjacencia[i][j]);
    }
  } 
  printf ("\n");*/

}

//------------------------------------------------------------------------------
int destroi_grafo(grafo g) {

  return g ? 0 : 1;
}
//------------------------------------------------------------------------------
grafo escreve_grafo(FILE *output, grafo g) {

  for (int i=0; i < numero_vertices; i++){
    fprintf(output, "\t\"%d %s\"\n", i, g->vertices[i]->nome);
  }
  return output ? g : NULL;
}
//------------------------------------------------------------------------------
char *nome(grafo g) {

  return g ? "" : "";
}
//------------------------------------------------------------------------------
unsigned int n_vertices(grafo g) {



  return g ? 0 : 0;
}

//------------------------------------------------------------------------------
int direcionado(grafo g) {

  return g ? 0 : 0;
}
//------------------------------------------------------------------------------
int conexo(grafo g) {

  return g ? 0 : 0;
}
//------------------------------------------------------------------------------
int fortemente_conexo(grafo g)  {

  return g ? 0 : 0;
}
//------------------------------------------------------------------------------
long int diametro(grafo g) {

  return g ? 0 : infinito;
}
//------------------------------------------------------------------------------

grafo distancias(grafo g) {

  return g;
}


void salva_vertices(vertice *vertices, Agraph_t *g_temp){
  // carrega lista de vertices 
  int i=0;

  for (Agnode_t *v=agfstnode(g_temp); v; v=agnxtnode(g_temp,v)) {
    vertices[i] = (vertice) malloc (sizeof(struct vertice));
    strcpy(vertices[i]->nome, agnameof(v));
    i++;
  }
}


 
// A utility function to find the vertex with minimum distance value, from
// the set of vertices not yet included in shortest path tree
int minDistance(long int dist[], bool sptSet[])
{
   // Initialize min value
   long int min = infinito;
   int min_index;
 
   for (int v = 0; v < numero_vertices; v++)
     if (sptSet[v] == false && dist[v] <= min)
         min = dist[v], min_index = v;
 
   return min_index;
}
 
// A utility function to print the constructed distance array
void printSolution(long int dist[], int n)
{
   printf("Vertex   Distance from Source\n");
   for (int i = 0; i < n; i++)
    if (dist[i] > 100000)
      printf("%d \t\t %s\n", i, "Infinito");
    else
      printf("%d \t\t %ld\n", i, dist[i]);
}

// Funtion that implements Dijkstra's single source shortest path algorithm
// for a graph represented using adjacency matrix representation
void dijkstra(grafo g, int src)
{
     int **graph = g->matriz_adjacencia;
     long int *dist = (long int *) malloc((long unsigned int)numero_vertices * sizeof(long int));     // The output array.  dist[i] will hold the shortest
                      // distance from src to i
 
     bool *sptSet = (bool *) malloc((long unsigned int)numero_vertices * sizeof(bool)); // sptSet[i] will true if vertex i is included in shortest
                     // path tree or shortest distance from src to i is finalized
 
     // Initialize all distances as INFINITE and stpSet[] as false
     for (int i = 0; i < numero_vertices ; i++)
        dist[i] = infinito, sptSet[i] = false;
 
     // Distance of source vertex from itself is always 0
     dist[src] = 0;
 
     // Find shortest path for all vertices
     for (int count = 0; count < numero_vertices-1; count++){
       // Pick the minimum distance vertex from the set of vertices not
       // yet processed. u is always equal to src in first iteration.
       int u = minDistance(dist, sptSet);
 
       // Mark the picked vertex as processed
       sptSet[u] = true;
 
       // Update dist value of the adjacent vertices of the picked vertex.
       for (int v = 0; v < numero_vertices; v++)
 
         // Update dist[v] only if is not in sptSet, there is an edge from
         // u to v, and total weight of path from src to  v through u is
         // smaller than current value of dist[v]
         if (!sptSet[v] && graph[u][v] && dist[u] != infinito
                                       && dist[u]+graph[u][v] < dist[v])
            dist[v] = dist[u] + graph[u][v];
     }
 
     // print the constructed distance array
     //printSolution(dist, numero_vertices);
}

