#include <stdio.h>
#include "cronometro.h"
#include "grafo.h"

//------------------------------------------------------------------------------
int main(void) {

  grafo g = le_grafo(stdin);

  cronometro_dispara();

  long int d = diametro(g);
  unsigned long int nsecs = cronometro();

  printf("%s\n", nome(g));

  printf("%sdirecionado\n", direcionado(g) ? "" : "não ");

  if ( direcionado(g) )
    
    printf("%sfortemente conexo\n", conexo(g) ? "" : "não ");
  
  else
    
    if ( conexo(g) )
      
      printf("%sconexo\n", conexo(g) ? "" : "des");
  
  if ( d == infinito )
    printf("diâmetro: oo\n");
  else
    printf("diâmetro: %ld\n", d);

  unsigned int n = n_vertices(g);

  printf("%u vértices\n", n);

  printf("%lu nanosegundos\n", nsecs);

  printf("%.2f ns/v^4\n", ((double)nsecs)/((double)(n*n*n*n)));

  destroi_grafo(escreve_grafo(stdout, distancias(g)));


  return ! destroi_grafo(g);
}
