#define _POSIX_C_SOURCE 199309L // ou dá erro na compilação?
#include <time.h>
#include "cronometro.h"

//------------------------------------------------------------------------------
#ifdef __MACH__
#include <mach/clock.h>
#include <mach/mach.h>

#ifndef CLOCK_PROCESS_CPUTIME_ID
#define CLOCK_PROCESS_CPUTIME_ID 2
#endif

static int clock_gettime(int clk_id, struct timespec *ts) {

  clock_serv_t cclock;
  mach_timespec_t mts;

  host_get_clock_service(mach_host_self(), CALENDAR_CLOCK, &cclock);
  clock_get_time(cclock, &mts);
  mach_port_deallocate(mach_task_self(), cclock);

  ts->tv_sec = mts.tv_sec;
  ts->tv_nsec = mts.tv_nsec;
  return 0;
}
#endif

//------------------------------------------------------------------------------
static struct timespec antes;

//------------------------------------------------------------------------------
int cronometro_dispara(void) {
    
  return clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &antes);
}
//------------------------------------------------------------------------------
unsigned long int cronometro(void) {

  struct timespec agora;

  clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &agora);

  unsigned long int 
    sec = (unsigned long int)agora.tv_sec - (unsigned long int)antes.tv_sec,
    nsec = (unsigned long int)agora.tv_nsec - (unsigned long int)antes.tv_nsec;

  return sec*1000000000 + nsec;
}
