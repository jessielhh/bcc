#ifndef _CRONOMETRO_H
#define _CRONOMETRO_H


//-----------------------------------------------------------------------------
// "zera" o cronometro e inicia a contagem de tempo
// devolve o valor devolvido por clock_gettime()

int cronometro_dispara(void);

//-----------------------------------------------------------------------------
// devolve o número de nanosegundos decorridos
// desde a última chamada de cronometro_dispara() 

unsigned long int cronometro(void);

#endif
